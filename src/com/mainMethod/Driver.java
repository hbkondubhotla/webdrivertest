package com.mainMethod;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Driver {
	
	@Test(groups={"webdrivertest"})
	public static void chromeDriver(){
		WebDriver d;
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Workspace\\com.webdriver.testng1\\webdrivertestng trial\\Resources\\chromedriver.exe");
		d = new ChromeDriver();
		d.get("https://www.google.com");
		d.quit();
	}
}
